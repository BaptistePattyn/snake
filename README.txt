Welcome to the game called snakepong by Baptiste Pattyn

the game can be played with 2 players on one operating system
The playing field gets loaded in from a txt file, this can be changed by the players and players can even create new ones
if they want to.

Controls:
	snake: use the arrow keys to change the direction of the snake
	paddle: use w to move up and s to move down
		note: when you stop pushing w or s the paddle stops moving

There is no score system implemented yet, the game can end in two ways (it will spit out errors and stop :D )
1) snake hits a wall or eats itself (snake loses)
2) ball passes the paddle (snake wins)

In future versions I would implement a GUI with different maze levels.
I would alo let the speed of the snake get faster over time to add difficulty
