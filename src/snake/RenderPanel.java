package snake;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
import javax.swing.*;

import java.util.Random;
/**
 * class that renders the game and where the collisiondetection is coded, the game variables are given coords, the maze is generated and everything is panted
 * @author patty
 *
 */
@SuppressWarnings("serial")
public class RenderPanel extends JPanel implements ActionListener, KeyListener{
	private static Color background= new Color(151326);
	private int cols, blockHeight, blockWidth;
	public int rows;
	private int[][] mazeArray;
	private JFrame frame;
	private double screenWidth;
	private double screenHeight;
	private Snake<int[]> snake;
	private Paddle<int[]> paddle;
	private Food food = new Food();
	private Ball ball;
	private Random random = new Random();
	/**
	 * constructor of renderpanel with snake, paddle and ball int it
	 * @param snake snake of the game
	 * @param paddle paddle of the game
	 * @param ball of the game
	 */
	public RenderPanel(Snake<int[]> snake, Paddle<int[]> paddle, Ball ball) {
		
		this.mazeArray = getMazeArray();
		this.blockWidth = 30;
		this.blockHeight = 30;
		this.screenWidth = (blockWidth) * cols + 5 ;
		this.screenHeight = (blockHeight) * rows + blockHeight + 5 ;
		this.setBackground(background);
		frame = new JFrame();
		frame.setSize((int) (screenWidth), (int) (screenHeight));
		frame.setContentPane(this);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("SNAKEPONG");
		frame.setResizable(false);
		frame.setVisible(true);
		addKeyListener(this);
		setFocusable(true);
		frame.validate();
		this.repaint();
		frame.repaint();
		this.ball = ball;
		this.paddle = paddle;
		this.snake = snake;
		
	}
	/**
	 * method that gets called once in the beginning to give snake and paddle coords
	 */
	public void init() {
		this.snake.setHead(new int[] {cols/2 , rows/2});
		this.paddle.setHead(new int[] {cols-1, rows/2+2});
		birthOfItems();
	}
	/**
	 * generates random coords for the food to spawn within the maze, not on the walls and not on the paddle or the snake
	 * @return rankdom coords
	 */
	private int[] generateRandomCoord() {
		int[] coord = new int[2] ;
		do {
			coord = new int[] {random.nextInt(cols-2)+1, random.nextInt(rows-2)+1};
			System.out.println("RANDOM COORD");
			System.out.println(coord[0]+" " + coord[1]);
			
		} while(mazeArray[coord[1]][coord[0]] == 1);
		return coord ;

	}
	/**
	 * generate extra nodes for the snake and the paddle
	 */
	public void birthOfItems() {
		int[] snakeCoord = snake.getHead();
		int[] paddleCoord = paddle.getHead();
		for(int i = 0; i < 4; i++) {
			System.out.println(i);
			this.snake.prepend(snakeCoord);
			this.paddle.append(paddleCoord);
			snakeCoord = new int[] {snakeCoord[0]+1,snakeCoord[1]};
			paddleCoord = new int[] {paddleCoord[0], paddleCoord[1]+1};
		}
		this.ball.setCoord(new int[] {cols-2, rows/2});
		int[] coord = generateRandomCoord();
		this.food.setCoord(coord);
		this.paddle.setMoving(true);
		this.paddle.setDir(Direction.Up);
		this.paddle.move();
		this.paddle.move();
		this.paddle.move();
		this.paddle.move();
		this.paddle.setMoving(false);
		repaint();
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
	}
	/**
	 * paint all of the components on the panel
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.BLACK);
		int xcoord = 0;
		int ycoord = 0;
		for(int i = 0; i < this.rows; i++) {
			for(int j = 0; j< this.cols; j++) {
				if(mazeArray[i][j] == 1) {
					g.fillRect(xcoord*blockWidth, ycoord*blockHeight, this.blockWidth, this.blockHeight);
				}
				xcoord++;
			}
			xcoord = 0;
			ycoord++;
		}	
		
		g.setColor(Color.WHITE);
		g.fillRect(food.getCoord()[0]*blockWidth, food.getCoord()[1]*blockHeight, blockWidth, blockHeight);
		
		
		g.setColor(Color.YELLOW);
		g.fillOval(ball.getCoord()[0]*blockWidth, ball.getCoord()[1]*blockHeight, blockWidth, blockHeight);
		
		

		
		for( int[] coord : this.snake) {
			g.setColor(Color.RED);
			g.fillRect(coord[0]*blockWidth, coord[1]*blockHeight, blockWidth, blockHeight);
			g.setColor(Color.BLACK);
			g.drawRect(coord[0]*blockWidth, coord[1]*blockHeight, blockWidth, blockHeight);
		}
		

		for(int[] coord: this.paddle) {
			g.setColor(Color.BLUE);
			g.fillRect(coord[0]*blockWidth, coord[1]*blockHeight, blockWidth, blockHeight);
			g.setColor(Color.BLACK);
			g.drawRect(coord[0]*blockWidth, coord[1]*blockHeight, blockWidth, blockHeight);
		}
		
		
		

	}
	/**
	 * read an included text-file that contains blank spaces and #-symbols, the blank spaces are blank spaces on the play field and the #-symbols stand for walls
	 * the method reads the file and transfers the data into a 2d array that represents the play field
	 * this array will be used for collision detection and to paint the maze
	 * you can make your own mazes if you want but keep in mind that the number of cols and rows depends on your screen resolution, you can alwasy adjust blockheight and blockwidth to fit a more complex maze 
	 */
	public int[][] getMazeArray(){
	String FILENAME = "C:\\Users\\patty\\OneDrive\\algoritmen\\Sake2.0\\src\\testmaze.txt";
	String mazeString = "";
	BufferedReader br = null;
	FileReader fr = null;
	int[][] mazeArray;
	
	try {
		fr = new FileReader(FILENAME);
		br = new BufferedReader(fr);
		
		String sCurrentLine;
		while ((sCurrentLine = br.readLine()) != null) {
			cols = sCurrentLine.length();
			rows++;
			mazeString += (sCurrentLine + System.lineSeparator());
		}
	} catch(IOException e){
		e.printStackTrace();
	} 	
	mazeArray = new int[rows][cols];
	Scanner scanner = new Scanner(mazeString); 
	int i = 0;
	while(scanner.hasNextLine()) {
		String line = scanner.nextLine();
		
		for (int j = 0; j < cols; j++) {
			char c = line.charAt(j);
			if((int)c == 35) {
				mazeArray[i][j] = 1;
			} else {
				mazeArray[i][j] = 0;
			}
			//System.out.print(mazeArray[i][j]);
		}
		//System.out.println("");
		i++;
	}
	scanner.close();
	
	//System.out.println(mazeString);
	/*for(int i1 = 0; i1 < rows; i1++ ) {
		System.out.println(System.lineSeparator());
		for (int j = 0; j < cols; j++) {
			System.out.print(mazeArray[i1][j]);
		}
	}
	*/
	return mazeArray;
	}
 /**
  * function that returns the dimensions of the maze array
  * @return
  */
	public int[] getDimension() {
		int[] dim = new int[2];
		dim[0] = this.mazeArray.length;
		dim[1] = this.mazeArray.length;
		return dim;
	}
	/**
	 * function that detects if the snake is eating
	 * @return true or false
	 */
	public boolean eatDetection(){
		if(food.getCoord()[0] == snake.getX() && food.getCoord()[1] == snake.getY()) {
			int[]coord = generateRandomCoord();
			food.setCoord(coord);
			return true;
		} else {
			return false;
		}
	}
	/**
	 * funtion that detects if the snake is hitting a wall
	 * @return
	 */
	public boolean snakeCollDetection(){
		if(mazeArray[snake.getY()][snake.getX()] == 1 || snake.coordInSnake(snake.getHead())) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * funtion that detects if the ball is hitting a wall, the snake or the paddle
	 * @return
	 */
	public String ballCollDetection() {
		
		String returnString = "";
		
		int[] ball = this.ball.getCoord();
		
		int[] left = new int[] {ball[0]-1,ball[1]};
		int[] right = new int[] {ball[0]+1,ball[1]};
		int[] up = new int[] {ball[0],ball[1]-1};
		int[] down = new int[] {ball[0],ball[1]+1};
		
		
		if((mazeArray[left[1]][left[0]] == 1 || snake.coordInSnake(left) || paddle.coordInPaddle(left) || compareArrays(left, snake.getCoord()) ) && this.ball.getXDirection() == Direction.Left) {
			returnString += "left";
		} if ((mazeArray[right[1]][right[0]] == 1 || snake.coordInSnake(right) || paddle.coordInPaddle(right) || compareArrays(right, snake.getCoord())) && this.ball.getXDirection() == Direction.Right ) {
			returnString += "right";
		} if ((mazeArray[up[1]][up[0]] == 1 || snake.coordInSnake(up) || paddle.coordInPaddle(up) || compareArrays(up, snake.getCoord())) && this.ball.getYDirection() == Direction.Up) {
			returnString += "up";
		} if((mazeArray[down[1]][down[0]] == 1 || snake.coordInSnake(down) || paddle.coordInPaddle(down) || compareArrays(down, snake.getCoord())) && this.ball.getYDirection() == Direction.Down) {
			returnString +="down";
		} 
		else {returnString.concat("noColl");}
		return returnString;
	}
	/** 
	 * method that compares 2 arrays and returns true if they are equal
	 * @param array1
	 * @param array2
	 * @return true or false
	 */
	private boolean compareArrays(int[] array1, int[] array2) {
		boolean b = true;
        if (array1 != null && array2 != null){
          if (array1.length != array2.length)
              b = false;
          else
              for (int i = 0; i < array2.length; i++) {
                  if (array2[i] != array1[i]) {
                      b = false;    
                  }                 
            }
        }else{
          b = false;
        }
		return b;
	}
	
	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	/**
	 * check for pressed keys to handle the user input
	 */
	@Override
	public void keyPressed(KeyEvent e) {
		int key = e.getKeyCode();
		
		switch(key){
		case KeyEvent.VK_LEFT:
			if(snake.getDirection() != Direction.Right) {

				snake.setDirection(Direction.Left);
			}
			break;
		case KeyEvent.VK_RIGHT:
			if(snake.getDirection() != Direction.Left) {

				snake.setDirection(Direction.Right);
			}
			break;
		case KeyEvent.VK_UP:
			if(snake.getDirection() != Direction.Down) {

				snake.setDirection(Direction.Up);
			}
			break;
		case KeyEvent.VK_DOWN:
			if(snake.getDirection() != Direction.Up) {

				snake.setDirection(Direction.Down);
			}
			break;
		case KeyEvent.VK_W:
			paddle.setMoving(true);
			paddle.setDir(Direction.Up);
			break;
		case KeyEvent.VK_S:
			paddle.setMoving(true);
			paddle.setDir(Direction.Down);
			break;
		}
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		int key = e.getKeyCode();
		if(key == KeyEvent.VK_W || key == KeyEvent.VK_S) {
			paddle.setMoving(false);
		}
		
	}
}
