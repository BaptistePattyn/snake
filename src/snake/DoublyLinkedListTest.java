package snake;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class DoublyLinkedListTest {
	
	DoublyLinkedList<int[]> dll1;
	DoublyLinkedList<String> dll2;

	@BeforeEach
	void setUp() throws Exception {
		dll1 = new DoublyLinkedList<>(new int[] {10,10});
		dll2 = new DoublyLinkedList<>();
	}

	@Test
	void testGetSize() {
		assertEquals(1, dll1.getSize());
		assertEquals(0, dll2.getSize());
	}
	
	@Test
	void testIsEmpty() {
		assertEquals(true, dll2.isEmpty());
		assertEquals(false, dll1.isEmpty());
	}
	
	@Test
	void testPrepend() {
		dll2.prepend("hallo");
		assertEquals("hallo", dll2.getHead());
		int[] coord1 = new int[] {20,20};
		int[] coord2 = new int[] {30,30};
		dll1.prepend(coord1);
		assertEquals(coord1, dll1.getHead());
		dll1.prepend(coord2);
		assertEquals(coord2, dll1.getHead());
	}
	
	@Test
	void testAppend() {
		dll1.append(new int[] {20,20});
		dll1.append(new int[] {80,80});
		dll1.append(new int[] {100,100});
		assertEquals(100, dll1.getTail()[0]);
		dll1.removeTail();
		dll1.removeTail();
		assertEquals(20, dll1.getTail()[0]);
	}
	
	@Test
	void testRemoveTail() {
		dll1.prepend(new int[] {20,20});
		dll1.prepend(new int[] {80,80});
		dll1.removeTail();
		dll1.removeTail();
		assertEquals(80, dll1.getTail()[0]);
	}
	
	@Test
	void testRemoveHead() {
		dll1.prepend(new int[] {20,20});
		dll1.prepend(new int[] {80,80});
		dll1.removeHead();
		dll1.removeHead();

		assertEquals(10, dll1.getHead()[0]);
	}
	
	@Test
	void testGetHead() {
		assertEquals(10,dll1.getHead()[0]);
	}
	
	
}
