package snake;


import java.awt.event.*;
import javax.swing.Timer;

/**
 * class that implements the game with a timer 
 * @author Baptiste Pattyn
 *
 */
public class Game implements ActionListener{
	private RenderPanel renderPanel;
	private Snake<int[]> snake;
	private Paddle<int[]> paddle;
	private Ball ball;
	private Timer timer = new Timer(20, this);
	public int ticks = 0; 
	
	public Game() {
		init();
		timer.start();
	}
	/** 
	 * method that gets called at the start of a game and that initializes the renderpanel, the snake, the paddle and the ball
	 * this method calls the init method from renderpanel
	 */
	public void init() {
		
		this.snake =  new Snake<int[]>(new int[] {1,1});
		this.paddle = new Paddle<int[]>(new int[] {1,1});
		this.ball = new Ball(new int[] {5,5});
		this.renderPanel = new RenderPanel(this.snake, this.paddle, this.ball);
		this.renderPanel.init();
	}
	
	public static void main(String[] args) {
		Game game = new Game();
	}
	/**
	 * override the actionperformed to use the timer to trigger events, we use only one timer but snake is moving twice as fast as the ball and the paddle
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		ticks++;
		if(ticks % 10 ==0 ) {
			timer.stop();	
			/** snake movement**/
			if(renderPanel.snakeCollDetection()) {
				System.out.println("YOU DIED");
			}
			snake.move(renderPanel.eatDetection());
			
			timer.restart();
		}if(ticks % 20 == 0) {
			timer.stop();
			/** paddle movement**/
			if(paddle.getMoving()) {
				if(!(paddle.getY() == 0 && paddle.getDir() == Direction.Up) || !(paddle.getTail()[1] == renderPanel.rows && paddle.getDir() == Direction.Down )) {
					paddle.move();
					System.out.println(paddle.size);
				}
			}
			/** ball movement**/
			String ballCollision = renderPanel.ballCollDetection();
			if(!ballCollision.equals("noColl")) {
				ball.changeMoveDirection(ballCollision);
			}
			ball.move();
			timer.restart();
		}
		renderPanel.repaint();
		
	}

	
	
}
