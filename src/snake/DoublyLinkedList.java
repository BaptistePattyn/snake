package snake;

import java.util.Iterator;

/**
 * 
 * @author Baptiste Pattyn
 * 
 * Doubly linked list implementation
 *
 * @param E
 */

public class DoublyLinkedList<E> implements Iterable<E> {
	protected Node head;
	protected Node tail;
	protected int size;
	
	/**
	 *  Constructs an empty Doubly Linked List
	 */
	public DoublyLinkedList(){
		this.head = null;
		this.tail = null;
		this.size = 0;
	}
	
	/**
	 * Constructs a Doubly Linked List with one node that has an element
	 * 
	 * @param element of the node in the list
	 */
	
	public DoublyLinkedList(E element) {
		this.size = 1;
		this.head = new Node(null,element, null);
		this.tail = null;
	}
	
	/*public DoublyLinkedList(Node node, int size) {
		this.size = size;
		this.head = node;
		this.tail = head.getNext();
	}*/
	
	/**
	 * 
	 * @return the number of nodes in the Doubly Linked List
	 */
	
	public int getSize() {
		return size;
	}

	/**
	 * 
	 * @return if the doubly linked list is empty
	 */
	
	public boolean isEmpty() {
		return size == 0;
	}
	
	/**
	 * 
	 * @param element to prepend to the Doubly Linked List
	 */
	public void prepend(E element) {
		//System.out.println("prepend");
		Node temp = new Node(element);
		    if(head == null) {
		    	head = temp;
		    }if(tail == null){
		    	tail = head;
		    	head = temp;
		    	head.setNext(tail);
		    	tail.setPrev(head);
		    }else {
		    	temp.setNext(head);
		    	head.setPrev(temp);
		    	temp.setPrev(null);
		    	head = temp;
		    	
		    }
		    size++;
	}
	
	public void append(E element) {
		Node temp = new Node(element);
		if(head ==  null) {
			head = temp;
			tail = head;
		} if(tail == null) {
			tail = temp;
			head.setNext(tail);
			tail.setPrev(head);
		} else {
			tail.setNext(temp);
			temp.setPrev(tail);
			temp.setNext(null);
			tail = temp;
			
		}
		size++;
	}
	
	/**
	 * 
	 * @return the element in the head of the Doubly Linked List
	 */
	
	public E getHead() {
		return head.getElement();
	}
	
	public void setHead(E element) {
		head.setElement(element);
	}
	
	/**
	 * 
	 * @return the element in the tail of the Doubly Linked List
	 */
	public E getTail() {
		return tail.getElement();
	}
	
	public Node getTailNode() {
		return tail;
	}
	
	public Node getHeadNode() {
		return head;
	}
	/*
	 * removes the tail of a doubly linked list
	 */
	
	public void removeTail() {		
		this.tail = this.tail.getPrev();
		this.tail.setNext(null);
		this.size--;
		
	}	
	/**
	 * removes the head of a doubly linked list
	 */
	
	public void removeHead() {
		this.head = this.head.getNext();
		this.head.setPrev(null);
		this.size--;
	}
		
		@Override
		public Iterator<E> iterator(){
			return new DoublyLinkedListIterator();	
		}
		
		private class DoublyLinkedListIterator implements Iterator<E>{
			private Node cursor = head;
			@Override
			public boolean hasNext() {
				return cursor != null;
			}
			
			@Override
			public E next() {
				E element = cursor.getElement();
				cursor = cursor.getNext();
				return element;
			}
			
		}
		
		/**
		 * This is a node conataining an element used in a doubly Linked List
		 *
		 */
		private class Node {
			private E element;
			private Node next;
			private Node prev;
			
			public Node(E element) {
				this.element = element;
			}
			
			/**
			 * Creates a node with a single element
			 * next referring to the next node
			 * prev referring to the previous node
			 * 
			 *  @param element2
			 *  @param next
			 *  @param prev
			 */
			
			public Node( Node prev, E element, Node next) {
				this.setElement(element);
				this.next = next;
				this.prev = prev;
			}
			
			/**
			 * Creates a node with a single element
			 * next referring to null
			 * prev referring to the previous node
			 * 
			 *  @param element
			 *  @param next
			 *  @param prev
			 */
			
			public Node(E element, Node prev) {
				this.setElement(element);
				this.next = null;
				this.prev = prev;
			}
			
			/**
			 * Getter for the previous node
			 * 
			 * @return previous node
			 */
			
			public Node getPrev() {
				return this.prev;
			}
			
			/**
			 * Setter for the previous node
			 * 
			 * @param prev
			 */
			
			public void setPrev(Node prev) {
				this.prev = prev;
			}
			
			/**
			 * Getter for the next node
			 * 
			 * @return next node
			 */
			
			public Node getNext() {
				return this.next;
			}
			
			/**
			 * Setter for the next node
			 * 
			 * @param next
			 */
			
			public void setNext(Node next) {
				this.next = next;
			}
			
			/**
			 * Getter for the element
			 * 
			 * @return element
			 */

			public E getElement() {
				return element;
			}
			
			/**
			 * Setter for the element
			 * 
			 * @param element
			 */

			public void setElement(E element) {
				this.element = element;
			}
		}
	}
	
	

