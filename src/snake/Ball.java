package snake;
/**
 * 
 * @author patty
 *
 * class that implements a ball that bounces around in the playfield
 */
public class Ball {
	
	private Direction xdir,ydir;
	private int[] coord;
	
	/**
	 * 
	 * @param coord the coords of the ball
	 */
	public Ball(int[] coord) {
		this.coord = coord;
		this.xdir = Direction.Left ;
		this.ydir = Direction.Up;
	}
	/**
	 * setter for the coords of the ball
	 * @param coord coords of the ball
	 */
	public void setCoord(int[] coord) {
		this.coord = coord;
	}
	/**
	 * getter for the coords of the ball
	 * @return
	 */
	public int[] getCoord() {
		return coord;
	}
	/**
	 * method for moving the ball and giving it new coords depending on the direction
	 */
	public void move() {
		int xstep = 0;
		int ystep = 0;
		switch(this.getXDirection()) {
		case Left:
			xstep = -1;
			break;
		case Right:
			xstep = 1;
			break;
		case Up:
			break;
		case Down:
			break;
		}
		switch(this.getYDirection()) {
		case Up:
			ystep = -1;
			break;
		case Down:
			ystep = 1;
			break;
		case Left:
			break;
		case Right:
			break;
		}
		this.coord = new int[] {this.getCoord()[0] + xstep , this.getCoord()[1] + ystep};
	}
	/**
	 * change the move direction of the ball
	 * @param string tells ho to change the direction
	 */
	public void changeMoveDirection(String string) {
		switch(string) {
		case "left":
			this.xdir = Direction.Right;
			break;
		case "right":
			this.xdir = Direction.Left;
			break;
		case "up":
			this.ydir = Direction.Down;
			break;
		case "down":
			this.ydir = Direction.Up;
			break;
		case "leftup":
			this.xdir = Direction.Right;
			this.ydir = Direction.Down;
			break;
		case "leftdown":
			this.xdir = Direction.Right;
			this.ydir = Direction.Up;
			break;
		case "rightup":
			this.xdir = Direction.Left;
			this.ydir = Direction.Down;
			break;
		case "rightdown":
			this.xdir = Direction.Left;
			this.ydir = Direction.Up;
			break;
		}
	}
	/**
	 * get the xdirection from the ball
	 * @return
	 */
	public Direction getXDirection() {
		return xdir;
	}
	/**
	 * get the ydirectin from the ball
	 * @return
	 */
	public Direction getYDirection() {
		return ydir;
	}
}
