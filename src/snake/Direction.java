package snake;
/**
 * 
 * @author patty
 * 
 * enum class with 4 possible directions
 *
 */
public enum Direction {
	Left,
	Right,
	Up,
	Down;
	}
