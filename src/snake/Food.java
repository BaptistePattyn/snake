package snake;
/**
 * 
 * @author patty
 *
 * class for food for the snake
 */
public class Food {
	private int[] coord;
	
	public Food() {
		
	}
	/**
	 * setter for the coords of the food 
	 * @param coord coord of the food object
	 */
	public void setCoord(int[] coord) {
		this.coord = coord;
	}
	
	/**
	 * getter for the coords of the food
	 * @return coords of the food object
	 */
	public int[] getCoord() {
		return coord;
	}
	
}
