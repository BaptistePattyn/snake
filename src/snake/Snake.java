package snake;

import java.util.Iterator;
/**
 * class that implements a snake. following an implementation of DoublyLinkedList
 * @author patty
 *
 * @param <E>
 */
public class Snake<E> extends DoublyLinkedList<int[]> {
	private Direction dir;
	
	/**
	 * constructor for the snake with startcoord and a set the start direction to right
	 * @param element the startcoords
	 */
	public Snake(int [] element) {
		super(element);
		dir = Direction.Right;
	}
	/**
	 * get the xcoord of the head of the snake
	 * @return xcoord
	 */
	
	public int getX() {
		return getHead()[0];
	}
	/**
	 * get the ycoord of the head of the snake
	 * @return ycoord
	 */
	public int getY() {
		return getHead()[1];
	}
	/**
	 * get the coord of the head of the snake
	 * @return coord
	 */
	public int[] getCoord() {
		return getHead();
	}
	/**
	 * move the snake according to the direction it has
	 * @param grow determine if the snake has to grow or just move
	 */
	public void move(Boolean grow) {
		switch(dir) {
		case Up:
			prepend(new int[] {getX(),getY()-1});
			break;
		case Down:
			prepend(new int[] {getX(),getY()+1});
			break;
		case Right:
			prepend(new int[] {getX()+1,getY()});
			break;
		case Left:
			prepend(new int[] {getX()-1,getY()});
			break;
		}
		if(!grow) {
			removeTail();
		}	
	}
	/**
	 * get the direction of the snake
	 * @return direction of the snake
	 */
	public Direction getDirection(){
		return this.dir;
	}
	/**
	 * set the direction of the snake
	 * @param dir
	 */
	public void setDirection(Direction dir) {
		this.dir = dir;
	}
	
	/**
	 * method to check if certain coords are containted by the snake
	 * @param coord coords to check
	 * @return true or false
	 */
	public boolean coordInSnake(int[] coord) {
		Boolean send = false;
		int[] cursorCoords = new int[2];
		Iterator<int[]> iterator = this.iterator();
		iterator.next();
		while(iterator.hasNext()) {
			cursorCoords = iterator.next();
			if(cursorCoords[0] == coord[0] && cursorCoords[1] == coord[1]) {
				send = true;
				break;
			}
		}
		return send;
	}
	
}
