package snake;

import java.util.Iterator;

/**
 * 
 * @author Baptiste Pattyn
 *
 * @param <E>
 */
public class Paddle<E> extends DoublyLinkedList<int[]> {
	private Direction dir;
	private Boolean moving = false;
	
	/**
	 * constructor that gives a coord to the paddle and sets the direction to up
	 * @param coord
	 */
	public Paddle(int[] coord) {
		super(coord);
		dir = Direction.Up;
	}
	/**
	 * getter for the direction of the paddle
	 * @return the direction of the paddle
	 * 
	 */
	public Direction getDir() {
		return this.dir;
	}
	/**
	 * setter for the direction of the paddle
	 * @param dir
	 */
	public void setDir(Direction dir) {
		this.dir = dir;
	}
	/**
	 * getter to know if the paddle is moving or not
	 * @return
	 */
	public boolean getMoving() {
		return moving;
	}
	/**
	 * set the parameter for the paddle to move or to stay still
	 * @param moving
	 */
	public void setMoving(Boolean moving) {
		this.moving = moving;
	}
	
	/**
	 * get the x coord from the head of the paddle
	 * @return
	 */
	public int getX() {
		return getHead()[0];
	}
	/**
	 * het the y coord from the head of the paddle
	 * @return
	 */
	public int getY() {
		return getHead()[1];
	}
	/**
	 * move the paddle acoording to the direction it has	
	 */
	public void move() {
			switch(dir) {
			case Up:
				prepend(new int[] {getX(),getY()-1});
				System.out.println("REMOVE TAIL FROM PADDLE");
				removeTail();
				break;
			case Down:
				append(new int[] {getTail()[0],getTail()[1]+1});
				System.out.println("REMOVE HEAD FROM PADDLE");
				removeHead();
				break;
			case Right: case Left:
				break;
			}
		
	}
	/**
	 * check if a given coord is in the paddle
	 * @param coord coord to check for
	 * @return true or false
	 */
	public boolean coordInPaddle(int[] coord) {
		Boolean send = false;
		int[] cursorCoords = new int[2];
		Iterator<int[]> iterator = this.iterator();
		while(iterator.hasNext()) {
			cursorCoords = iterator.next();
			if(cursorCoords[0] == coord[0] && cursorCoords[1] == coord[1]) {
				send = true;
				break;
			}
		}
		return send;
	}
}
	
